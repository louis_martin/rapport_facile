
# Utilisation

## Étapes initiales

1. Téléchargez le fichier `zip` du site <https://bitbucket.org/louis_martin/rapport_facile/downloads/> et décompresser son contenu.

1. Copiez le répertoire `exemple-base` là où vous le désirez.

1. Renommez le répertoire copié comme vous le désirez.

## Modification du fichier `document-base.md`

Le fichier `document-base.md` est le fichier principal gouvernant l'assemblage de votre rapport. Les premières lignes du fichier sont :

~~~ {#code_1 numbers=left caption="Premières lignes du fichier 'document-base.md'"}
---
bibliography: bibliographie.bib
link-citations: true
csl: iso690-author-date-fr-no-abstract-lm.csl
nocite: |
  @*
---
<!--
    Version du 2020-11-18 - Louis Martin
-->

<!-- Page de titre -->
\begin{titlepage}
    \begin{center}
    { \setstretch{1.2} \large
        \MakeUppercase{
            { \Large Université du Québec à Montréal }
            \vfill
            Titre long du rapport\break
            s'étendant sur plus d'une ligne
            \vfill
            Mémoire présenté\break
            comme exigence partielle\break
            de la maitrise en génie logiciel
            \vfill
            par \break Louis Martin
            \vfill
            novembre 2020
        }
    }
    \end{center}
\end{titlepage}
~~~

Modifier les valeurs à votre convenance.

Il est important que ces lignes soient exactement les premières lignes du fichier. La commande `\break` force une fin de ligne vous permettant de contrôler la disposition des titres longs.

Le reste du fichier se lit assez bien. En _Markdown Pandoc_, les commentaires débutent par la séquence de caractères `<!--` et se terminent par la séquence de caractères `-->`.

Pour inclure le contenu des fichiers du sous-répertoire `textes`, la commande à insérer à l'endroit voulu est du type :

    %inclure:textes/nom-du-fichier-a-inserer.md

Cette commande commence au tout début de la ligne sans espace. Il est utile de ne pas utiliser d'espace dans le nom des sous-répertoires et le nom des fichiers.

## Modification du fichier `preambule.txt`

Le fichier `preambule.txt` contient les commandes \LaTeX\ qui seront exécutées au tout début de la génération du rapport. Le fichier contient des commentaires expliquant le but de chaque segment. Ce fichier vous permet:

- d'inclure un filigrane ayant la mention _version projet_;
- de contrôler l'impression recto verso et de contrôler les ouvertures de chapitre du côté droit, gauche, etc.

__Petit rappel__: En \LaTeX, la mise en commentaire d'effectue en utilisant le caractère `%`.


## Rédaction

La rédaction s'effectue en éditant des fichiers au format _Markdown_, soit le fichier `document-base.md`, soit les fichiers que vous créerez dans le sous-répertoire `textes`. La syntaxe à utiliser est décrite sur le site de Pandoc <http://pandoc.org/MANUAL.html>.

## Génération du rapport

La génération du rapport s'effectue en lançant la commande suivante:

    $ ./pdf.sh document-base.md

Cette commande est lancée dans l'application _Terminal_ en s'étant positionné dans le répertoire racine de votre rapport. Le document PDF généré sera dans le répertoire courant sous le nom `document-base.pdf`.

Il se peut que des messages du type:

    [WARNING] Missing character: There is no ᵉ in font [lmroman10-regular]:mapping=tex-text;!

apparaissent. Ils n'ont pas d'impact.
