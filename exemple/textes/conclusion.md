# Conclusion

Exemple d'une table plus complexe codée en \LaTeX.

\singlespacing
\begin{table}[htbp]
\centering
\caption{Facteur --- Vision stratégique claire liée à l'approche Agile}
\begin{tabular}{p{6.5cm}p{6.5cm}}
   \toprule
   \rowcolor{\couleurtable}\multicolumn{2}{c}{\large \bfseries Vision stratégique claire liée à l'approche Agile} \\
   \rowcolor{\couleurtable}\multicolumn{2}{c}{ } \\
   \rowcolor{\couleurtable}\multicolumn{1}{c}{\bfseries Influence positive (+)} & \multicolumn{1}{c}{\bfseries Influence négative (--)}\\
   \midrule
   --- Le STIOM a une volonté de mettre en œuvre l'approche Agile au sein de ses divisions en énonçant sa vision globale et des buts. \newline
   --- La stratégie du STIOM se décline clairement à travers tous les niveaux hiérarchiques. \newline
   --- Les transformations à venir sur l'approche Agile au STIOM et son déroulement sont communiqués aux employés. \newline
   --- Les valeurs prônées par le STIOM sont claires et largement communiquées. \newline
   --- Les informations concernant le service TI et ses plans d'action sont diffusées à tous les niveaux, en des termes compréhensibles par tous.
   &
   --- La culture de l’organisation est une culture dite «de contrôle». \newline
   --- Pas de participation des employés sur les processus qui les affectent directement. \newline
   --- Chaque division gère son personnel en silo et l’affecte temporairement et à temps partiel à certains projets. Ce qui est un frein quand ces personnes n’ont pas les disponibilités suffisantes ni la vue d’ensemble du projet. \newline
   --- Une faible implication opérationnelle au niveau de la direction. Il faut dépasser le discours. \newline
   --- Les processus du STIOM ne permettent pas de prendre rapidement des décisions lorsque les circonstances changent.\\
   \bottomrule
\end{tabular}
\end{table}
\onehalfspacing


\singlespacing
\begin{table}[H]
\centering
\caption{Les 4 valeurs du Manifeste Agile (AgileAlliance, 2001)}
\begin{tabular}{p{15cm}}
   \toprule
   \rowcolor{\couleurtable}\multicolumn{1}{c}{\large \bfseries Les 4 valeurs du Manifeste Agile (AgileAlliance, 2001)} \\
   \rowcolor{\couleurtable}{ } \\
   \rowcolor{\couleurtable}{\bfseries Nous découvrons comment mieux développer des logiciels par la pratique et en aidant les autres à le faire. Ces expériences nous ont amenés à valoriser:} \\
   \midrule
   1. Les individus et leurs interactions plus que les processus et les outils. \newline
   2. Des logiciels opérationnels plus qu’une documentation exhaustive. \newline
   3. La collaboration avec les clients plus que la négociation contractuelle. \newline
   4. L’adaptation au changement plus que le suivi d’un plan. \\
   \midrule
   \rowcolor{\couleurtable}\multicolumn{1}{c}{\shortstack[c]{{\bfseries Nous reconnaissons la valeur des seconds éléments,} \\
       {\bfseries mais privilégions les premiers.}}}\\
   \bottomrule
\end{tabular}
\end{table}
\onehalfspacing


\singlespacing
\begin{table}[H]
\centering
\caption{Les 12 principes du Manifeste Agile (AgileAlliance, 2001)} \label{table 2.2}
\begin{tabular}{>{\bfseries}cp{10.5cm}}
   \toprule
   \rowcolor{\couleurtable}\multicolumn{2}{c}{\large \bfseries Les 12 principes du Manifeste Agile (AgileAlliance, 2001)} \\
   \rowcolor{\couleurtable}\multicolumn{2}{c}{ } \\
   \rowcolor{\couleurtable}\multicolumn{1}{c}{} & \multicolumn{1}{l}{\bfseries Nous respectons les principes suivants:}\\
   \midrule
   1. & Notre plus haute priorité est de satisfaire le client en livrant rapidement et régulièrement des fonctionnalités à grande valeur ajoutée. \\
   2. & Accueillez positivement les changements de besoins, même tard dans le projet. Les processus agiles exploitent le changement pour donner avantage compétitif au client. \\
   3. & Livrer fréquemment un logiciel opérationnel avec des cycles de quelques semaines à quelques mois et une préférence pour les plus courts. \\
   4. & Les utilisateurs ou leurs représentants et les développeurs doivent travailler ensemble quotidiennement tout au long du projet. \\
   5. & Réalisez les projets avec des personnes motivées. Fournissez-leur l’environnement et le soutien dont ils ont besoin et faites-leur confiance pour atteindre les objectifs fixés. \\
   6. & La méthode la plus simple et la plus efficace pour transmettre de l’information à l’équipe de développement et à l’intérieur de celle-ci est le dialogue en face à face. \\
   7. & Un logiciel opérationnel est la principale mesure d’avancement. \\
   8. & Les processus agiles encouragent un rythme de développement soutenable. Ensemble, les commanditaires, les développeurs et les utilisateurs devraient être capables de maintenir indéfiniment un rythme constant. \\
   9. & Une attention continue à l’excellence technique et une bonne conception renforce l’Agilité. \\
   10. & La simplicité — c’est-à-dire l’art de minimiser la quantité de travail inutile — est essentielle. \\
   11. & Les meilleures architectures, spécifications et conceptions émergent d’équipes auto-organisées. \\
   12. & À intervalles réguliers, l’équipe réfléchit aux moyens de devenir plus efficace, puis règle et modifie son comportement en conséquence. \\
   \bottomrule
\end{tabular}
\end{table}
\onehalfspacing


\singlespacing
\begin{table}[H]
\centering
\caption{Liste des 22 domaines de processus du CMMI™ version 1.3}\label{table 2.3}
\begin{tabular}{p{2.0cm}>{\small}p{2.0cm}>{\itshape}p{5.0cm}p{5.0cm}}
   \toprule
   \rowcolor{\couleurtable}\multicolumn{4}{c}{\large \bfseries Liste des 22 domaines de processus du CMMI™ version 1.3} \\
   \rowcolor{\couleurtable}\multicolumn{4}{c}{ } \\
   \rowcolor{\couleurtable}\multicolumn{1}{l}{\bfseries Catégories} & \multicolumn{1}{c}{\bfseries Acronymes} & \multicolumn{1}{l}{\bfseries Process Area} & \multicolumn{1}{l}{\bfseries Domaines de processus} \\
   \midrule
   \parbox[t]{2mm}{\multirow{7}{*}{\rotatebox[origin=c]{90}{\bfseries Gestion de projet}}}
    & REQM & Requirements Management & Gestion des exigences \\
    & PP & Project Planning & Planification de projet  \\
    & PMC & Project Monitoring \& Control  & Surveillance et contrôle de projet \\
    & SAM & Supplier Agreement Management & Gestion des accords avec les fournisseurs \\
    & IPM & Integrated Project Management & Gestion de projet intégrée \\
    & RSKM & Risk Management & Gestion des risques \\
    & QPM & Quantitative Project Management & Gestion de projet quantitative \\
    \midrule
   \parbox[t]{2mm}{\multirow{5}{*}{\rotatebox[origin=c]{90}{\bfseries Ingénierie}}} & RD & Requirements Definition & Développement des exigences \\
    & TS & Technical Solution & Solution technique \\
    & PI & Product Integration & Intégration produit \\
    & VER & Verification & Vérification \\
    & VAL & Validation & Validation \\
    \midrule
   \parbox[t]{2mm}{\multirow{5}{*}{\rotatebox[origin=c]{90}{\bfseries Gestion du processus}}} & OPD & Organizational Process Definition & Définition du processus organisationnel \\
    & OPF & Organizational Process Focus & Focalisation sur le processus organisationnel \\
    & OPP & Organizational Process Performance & Performance du processus organisationnel \\
    & OT & Organizational Training & Formation organisationnelle \\
    & OPM & Organizational Performance Management & Gestion de la performance organisationnelle \\
    \midrule
   \parbox[t]{2mm}{\multirow{5}{*}{\rotatebox[origin=c]{90}{\bfseries Soutien}}} & CM & Configuration Management & Gestion de configuration \\
    & PPQA & Process \& Product Quality Assurance & Assurance qualité processus et produit \\
    & MA & Measurement \& Analysis & Mesure et analyse \\
    & CAR & Causal Analysis \& Resolution & Analyse causale et résolution \\
    & DAR & Decision Analysis \& Resolution & Analyse et prise de décision \\
   \bottomrule
\end{tabular}
\end{table}
\onehalfspacing
